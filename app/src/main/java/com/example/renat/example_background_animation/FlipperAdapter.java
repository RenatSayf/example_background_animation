package com.example.renat.example_background_animation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

/**
 * TODO: AdapterViewFlipper: 5. адаптер для AdapterViewFlipper
 */

public class FlipperAdapter extends BaseAdapter
{
    private Context context;
    private int[] images;

    public FlipperAdapter(Context context, int[] images)
    {
        this.context = context;
        this.images = images;
    }

    @Override
    public int getCount()
    {
        return images.length;
    }

    @Override
    public Object getItem(int i)
    {
        return context.getResources().getDrawable(images[i]);
    }

    @Override
    public long getItemId(int i)
    {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        View adapterView = view;
        if (adapterView == null)
        {
            // предварительно создать макет адаптера в директории res/layout
            adapterView = LayoutInflater.from(context).inflate(R.layout.images_layout, null);
        }

        ImageView imageView1 = (ImageView) adapterView.findViewById(R.id.image_background_1);
        imageView1.setImageResource(images[i]);

        return adapterView;
    }
}
