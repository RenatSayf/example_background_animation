package com.example.renat.example_background_animation;


import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterViewFlipper;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.ViewAnimator;
import android.widget.ViewFlipper;

import java.util.Date;


/**
 * TODO: Fragment: 2. Средствами Android Studio добавить новый фрагмент c макетом, но без factrory method и без interface collback
 */
public class BackgroundFragm extends Fragment
{
    View fragmentView = null;
    Chronometer chronometer;

    // картинки для фона
    public static int[] imagesId = new int[]
            {
                    R.drawable.img_uk,
                    R.drawable.img_uk2,
                    R.drawable.img_uk3,
                    R.drawable.img_uk5,
                    R.drawable.img_uk4,
                    R.drawable.img_uk6,
                    R.drawable.img_uk7,
                    R.drawable.img_uk8,
                    R.drawable.img_uk9,
                    R.drawable.img_uk10,
                    R.drawable.img_uk11,
                    R.drawable.img_uk12,
                    R.drawable.img_uk13,
                    R.drawable.img_uk14,
                    R.drawable.img_uk15,
                    R.drawable.img_usa2,
                    R.drawable.img_usa3,
                    R.drawable.img_usa4
            };
    AdapterViewFlipper adapterViewFlipper; // TODO: AdapterViewFlipper: 1. Этот компонент будет использоваться для анимированного изменения фонового изображения

    public BackgroundFragm()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setRetainInstance(true); // TODO: Fragment 3. true - что бы фрагмент не пересоздавался при изменении конфигурации
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if (fragmentView == null)   // TODO: Fragment 4. Обязательная проверка, что бы не раздувать новый макет при повороте экрана
        {
            fragmentView = inflater.inflate(R.layout.fragment_background, container, false);
        }

        if (savedInstanceState == null) // проверка, что бы не перезапускался AdapterViewFlipper каждый раз при повороте экрана
        {
            chronometer = (Chronometer) fragmentView.findViewById(R.id.chronometer1);
            chronometer.start();

            adapterViewFlipper = (AdapterViewFlipper) fragmentView.findViewById(R.id.adapter_view_flipper);

            // TODO: AdapterViewFlipper: 7. создание адаптера и запуск анимации
            FlipperAdapter flipperAdapter = new FlipperAdapter(getActivity(), imagesId);
            adapterViewFlipper.setAdapter(flipperAdapter);
            adapterViewFlipper.setFlipInterval(10000);

            // предварительно создать в директории res новую директорию animator и добавить в нее ресурсы анимации (см. директорию res/animator)
            adapterViewFlipper.setInAnimation(getActivity(), R.animator.in_animator);
            adapterViewFlipper.setOutAnimation(getActivity(), R.animator.out_animator);
            adapterViewFlipper.setAutoStart(true);
        }

        return fragmentView;
    }

    @Override
    public void onPause()
    {
        super.onPause();

    }

}
