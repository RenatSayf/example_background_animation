# Пример анимированного изменения фонового изображения activity #
## Используемые компоненты: ##
* Fragment - для сохранения состояния при изменении конфигурации
* AdapterViewFlipper - для анимированного изменения фонового изображения

* *Картинки хранятся в ресурсах. В каждый интервал времени, в макете только одно изображение*
* *Утечек памяти нет*
### ***Режим выбора изображения:*** ###
* последовательный (в ветке master)
* случайный (в ветке new_branch)

1.![Screenshot_2017-04-04-19-32-30.png](https://bitbucket.org/repo/Mrgprk7/images/3674825791-Screenshot_2017-04-04-19-32-30.png)
2.![Screenshot_2017-04-04-19-32-39.png](https://bitbucket.org/repo/Mrgprk7/images/945423792-Screenshot_2017-04-04-19-32-39.png) 
3.![Screenshot_2017-04-04-19-32-52.png](https://bitbucket.org/repo/Mrgprk7/images/1591829340-Screenshot_2017-04-04-19-32-52.png)
4.![Screenshot_2017-04-04-19-33-00.png](https://bitbucket.org/repo/Mrgprk7/images/2149365201-Screenshot_2017-04-04-19-33-00.png)
5.![Screenshot_2017-04-04-19-33-09.png](https://bitbucket.org/repo/Mrgprk7/images/4201307387-Screenshot_2017-04-04-19-33-09.png)
6.![Screenshot_2017-04-04-19-33-14.png](https://bitbucket.org/repo/Mrgprk7/images/3672897227-Screenshot_2017-04-04-19-33-14.png)
7.![Screenshot_2017-04-04-19-33-18.png](https://bitbucket.org/repo/Mrgprk7/images/3935195530-Screenshot_2017-04-04-19-33-18.png)